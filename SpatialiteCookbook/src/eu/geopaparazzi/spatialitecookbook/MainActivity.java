package eu.geopaparazzi.spatialitecookbook;

import jsqlite.Exception;
import eu.geopaparazzi.library.util.ResourcesManager;
import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends Activity {

    @Override
    protected void onCreate( Bundle savedInstanceState ) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ResourcesManager resourcesManager = ResourcesManager.getInstance(this);
        if (!resourcesManager.getApplicationDir().exists()) {
            throw new RuntimeException();
        }
    }

    @Override
    public boolean onCreateOptionsMenu( Menu menu ) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }

    public void runTests( View view ) {

        try {
            EditText textView = (EditText) findViewById(R.id.editText1);

            StringBuilder sb = new StringBuilder();
            sb.append("Init database...\n");
            textView.setText(sb);
            DatabaseHandler dbHandler = new DatabaseHandler(this, sb);
            sb.append("Done...\n");
            textView.setText(sb);

            String result = dbHandler.queryVersions();
            textView.setText(result);

            result = dbHandler.queryComuni();
            textView.setText(result);

            result = dbHandler.queryComuniWithGeom();
            textView.setText(result);

            result = dbHandler.queryComuniArea();
            textView.setText(result);

            result = dbHandler.queryComuniNearby();
            textView.setText(result);

            result = dbHandler.doSimpleTransform();
            textView.setText(result);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
